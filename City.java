public class City{
  public String country;
  public String region;
  public String city;
  public String postal_code;
  public double lat;
  public double lng;
  public int mCode;
  public int aCode;
  
  public City(){
    this.country = null;
    this.city = null;
    this.region = null;
    this.postal_code = null;
    this.lat = null;
    this.lng = null;
    this.mCode = null;
    this.aCode = null;
  }
  
  public City(String country, String region, String city, String postal_code, double lat, double lng, int mCode, int aCode){
    this.country = country;
    this.city = city;
    this.region = region;
    this.postal_code = postal_code;
    this.lat = lat;
    this.lng = lng;
    this.mCode = mCode;
    this.aCode = aCode;
  }
	
  public String lat_str(){
    return lat == null ? "null" : Double.toString(lat);
  }
  
  public String lng_str(){
    return lng == null ? "null" : Double.toString(lng);
  }
  
  public String mCode_str(){
    return mCode == null ? "null" : Integer.toString(mCode);
  }
  
  public String aCode_str(){
    return aCode == null ? "null" : Integer.toString(aCode);
  }
}