/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author HannanKhan
 */
public class DB {

    /**
     * @param args the command line arguments
     */
	
	public Connection db = null;
	
	public DB(){
		Class.forName("com.mysql.jdbc.Driver");
	    this.db = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?" + "user=root&password=");
	}
	
   	
	
    /*public void delete(Connection conn, int regno)
    {
        try
        {
            Statement st = conn.createStatement();
            int mar = st.executeUpdate("Delete FROM university.student where Reg_no=" + Integer.toString(regno) + ";");
            if(mar == 1)
            {
                System.out.println();
                System.out.println("Deleted");
                System.out.println();
            }
            else
            {
                System.out.println();
                System.out.println("No Result Found to Delete");
                System.out.println();
            }
        }
        catch (Exception ex)
        {
            System.out.println("Exception..."+ex.getMessage());
        }
    }*/
    
    /*public static void search(Connection conn, int regno)
    {
        try
        {
            Statement st = conn.createStatement();
            ResultSet mar = st.executeQuery("Select * FROM university.student where Reg_no=" + Integer.toString(regno) + ";");
            System.out.println();
            System.out.println("--------------------------------------------------------------------------------");
            System.out.println("---------------------------------- Results  ------------------------------------");
            System.out.println("--------------------------------------------------------------------------------");
            while(mar.next())
            {
                System.out.print(mar.getInt(1)+"\t");	
                System.out.print(mar.getString(2)+"\t");
                System.out.print(mar.getString(3)+"\t");
                System.out.print(mar.getString(4)+"\t");
                System.out.print(mar.getString(5)+"\t");
                System.out.print(mar.getString(6)+"\t");
                System.out.print(mar.getString(7));
                System.out.println();
            }
            System.out.println("--------------------------------------------------------------------------------");
        }
        catch (Exception ex)
        {
            System.out.println("Exception..."+ex.getMessage());
        }
    }*/
    
    public void Insert(City record)
    {
        try
        {
            Statement st = db.createStatement();
            PreparedStatement statement = db.prepareStatement("Insert into city (country, region, city, postal_code, lat, lng, mCode, aCode) Values (?,?,?,?,?,?,?,?)");
            
            statement.setString(1, record.country);
            statement.setString(2, record.region);
            statement.setString(3, record.city);
            statement.setString(4, record.postal_code);
            statement.setDouble(5, record.lat);
            statement.setDouble(6, record.lng);
            statement.setInt(7, record.mCode);
            statement.setInt(8, record.aCode);
          
            statement.executeUpdate();
        }
        catch (Exception ex)
        {
            System.out.println("Exception..."+ex.getMessage());
        }
    }
    
}
