import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ReadCSV {

  /*public static void main(String[] args) {

	ReadCVS obj = new ReadCVS();
	obj.run();

  }*/

  public List<City> read(String csvFile) {

	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";
    LinkedList<City> dataList = new LinkedList<City>();

	try {

		br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {

		        // use comma as separator
			String[] data = line.split(cvsSplitBy);
            
            data[4] = data[4] == "" ? null : Double.parseDouble(data[4]);
            data[5] = data[5] == "" ? null : Double.parseDouble(data[5]);
			
			data[6] = data[6] == "" ? null : Integer.parseInt(data[6]);
			data[7] = data[7] == "" ? null : Integer.parseInt(data[7]);
            
            City n_city = new City(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7]);
            dataList.add(n_city);
		}

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	return dataList;
  }

}